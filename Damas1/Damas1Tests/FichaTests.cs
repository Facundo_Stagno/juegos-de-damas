﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Damas1.Tests
{
    [TestClass()]
    public class FichaTests
    {

        /*[TestMethod()]
        public void PiezaTest()
        {
            Assert.Fail();
        }*/
        [TestMethod()]
        public void moverIzqTest()
        {
            Ficha p = new Ficha(1, 1, true);
            p.moverIzq();

            Assert.IsTrue(p.getX() == 0 && p.getY() == 2);
        }
        [TestMethod()]
        public void moverDerTest()
        {
            Ficha p = new Ficha(1, 1, true);
            p.moverDer();

            Assert.IsTrue(p.getX() == 2 && p.getY() == 2);
        }

    }
}

